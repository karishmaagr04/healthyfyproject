package com.example.healthyfyproject

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.healthyfyproject.model.TimeValue

class BookingAdapter(private val timeValueList: ArrayList<TimeValue>) :
    RecyclerView.Adapter<BookingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookingViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.child_item_layout, parent, false)
        return BookingViewHolder(view)
    }

    override fun getItemCount(): Int {
        return timeValueList.size
    }

    override fun onBindViewHolder(holder: BookingViewHolder, position: Int) {
        holder.bindData(timeValueList[position])
    }
}