package com.example.healthyfyproject.helper

import com.example.healthyfyproject.model.BookingResponse
import com.example.healthyfyproject.model.TimeValue
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

object TimeUtils {

    private val dateFormatter = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH)
    const val DATE_FORMAT_WEEK = "EEE, dd"
    val hashmap: HashMap<String, ArrayList<TimeValue>> = HashMap()

    init {
        dateFormatter.timeZone = TimeZone.getTimeZone("UTC")
    }

    fun findDateAndTimeMapFromString(
        bookingResponse: List<BookingResponse>
    ): HashMap<String, ArrayList<TimeValue>> {
        try {

            for (i in 0..bookingResponse.size) {
                val startFormattedDate: Date? =
                    dateFormatter.parse(bookingResponse[i].startTime!!.split("+")[0])
                val endFormattedDate: Date? =
                    dateFormatter.parse(bookingResponse[i].endTime!!.split("+")[0])
                val sdf12Hours = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
                val dateFormatWeek = SimpleDateFormat(DATE_FORMAT_WEEK, Locale.ENGLISH)
                val timeValue = TimeValue(
                    sdf12Hours.format(startFormattedDate!!),
                    sdf12Hours.format(endFormattedDate!!)
                )
                val date = dateFormatWeek.format(startFormattedDate)
                if (hashmap.containsKey(date)) {
                    val timeValueMap: ArrayList<TimeValue> = hashmap.get(date)!!
                    timeValueMap.add(timeValue)
                    hashmap.set(date, timeValueMap)
                } else {
                    val timeValueMap: ArrayList<TimeValue> = ArrayList()
                    hashmap.put(date, timeValueMap)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return hashmap
    }
}