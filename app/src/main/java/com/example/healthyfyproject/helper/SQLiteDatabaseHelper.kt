package com.example.healthyfyproject.helper

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.healthyfyproject.model.BookingResponse
import java.util.*

//https://medium.com/@ssaurel/learn-to-save-data-with-sqlite-on-android-b11a8f7718d3
class SQLiteDatabaseHandler(context: Context) :
    SQLiteOpenHelper(context,
        DATABASE_NAME, null,
        DATABASE_VERSION
    ) {

    override fun onCreate(db: SQLiteDatabase) {
        val CREATION_TABLE = ("CREATE TABLE Bookings ( "
                + "slot_id INTEGER PRIMARY KEY AUTOINCREMENT, " + "end_time TEXT, "
                + "is_booked INTEGER, " + "is_expired INTEGER, " + "start_time TEXT, " + "username TEXT )")

        db.execSQL(CREATION_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // you can implement here migration process
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        this.onCreate(db)
    }

    fun deleteOne(bookingdata: BookingResponse) {
        // Get reference to writable DB
        val db = this.writableDatabase
        db.delete(TABLE_NAME, "slot_id = ?", arrayOf((bookingdata.slotId.toString())))
        db.close()
    }

    fun getBooking(id: Int): BookingResponse {
        val db = this.readableDatabase
        val cursor = db.query(
            TABLE_NAME, // a. table
            COLUMNS, // b. column names
            " slot_id = ?", // c. selections
            arrayOf(id.toString()), null, null, null, null
        )// d. selections args
        // e. group by
        // f. having
        // g. order by
        // h. limit

        cursor?.moveToFirst()

        val bookingdata = BookingResponse(
            (Integer.parseInt(cursor!!.getString(0))),
            cursor.getString(1)
//            Integer.parseInt(cursor.getString(2)),
//            Integer.parseInt(cursor.getString(3)),
//            cursor.getString(4),
//            cursor.getString(5)
        )

        return bookingdata
    }

    fun getAllBookings(): List<BookingResponse> {

        val bookings = LinkedList<BookingResponse>()
        val query = "SELECT  * FROM $TABLE_NAME"
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)
        var bookingdata: BookingResponse? = null

        if (cursor.moveToFirst()) {
            do {
                bookingdata = BookingResponse(
                    (Integer.parseInt(cursor!!.getString(0))),
                    cursor.getString(1)
//                    Integer.parseInt(cursor.getString(2)),
//                    Integer.parseInt(cursor.getString(3)),
//                    cursor.getString(4),
//                    cursor.getString(5)
                )

                bookings.add(bookingdata)
            } while (cursor.moveToNext())
        }

        return bookings
    }

    fun addBooking(bookingdata: BookingResponse) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_ID, bookingdata.slotId)
        values.put(KEY_END_TIME, bookingdata.endTime)
        values.put(KEY_IS_BOOKED, bookingdata.isBooked)
        values.put(KEY_START_TIME, bookingdata.startTime)
        values.put(KEY_IS_EXPIRED, bookingdata.isExpired)
        values.put(KEY_USERNAME, bookingdata.username)
        // insert
        db.insert(TABLE_NAME, null, values)
        db.close()
    }


    companion object {

        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "BookingDB"
        private val TABLE_NAME = "Bookings"
        private val KEY_ID = "slot_id"
        private val KEY_END_TIME = "end_time"
        private val KEY_IS_BOOKED = "is_booked"
        private val KEY_IS_EXPIRED = "is_expired"
        private val KEY_START_TIME = "start_time"
        private val KEY_USERNAME = "username"
        private val COLUMNS = arrayOf(
            KEY_ID,
            KEY_END_TIME,
            KEY_IS_BOOKED,
            KEY_IS_EXPIRED,
            KEY_START_TIME,
            KEY_USERNAME
        )
    }

}