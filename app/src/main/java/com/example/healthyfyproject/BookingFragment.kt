package com.example.healthyfyproject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.healthyfyproject.model.TimeValue
import kotlinx.android.synthetic.main.fragment_booking.*

class BookingFragment(private val timeValueList: ArrayList<TimeValue>) : Fragment() {
    var timeValueMorning: ArrayList<TimeValue> = ArrayList()
    var timeValueAfternoon: ArrayList<TimeValue> = ArrayList()
    var bookingAdapterMorning: BookingAdapter? = null
    var bookingAdapterNoon: BookingAdapter? = null
    private val upIcon by lazy {
        ContextCompat.getDrawable(
            context!!,
            R.drawable.ic_keyboard_arrow_up_24px
        )
    }
    private val downIcon by lazy {
        ContextCompat.getDrawable(
            context!!,
            R.drawable.ic_keyboard_arrow_down_24px
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_booking, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        for (i in 0..timeValueList.size - 1) {
            if (timeValueList[i].startTime.toUpperCase().contains("AM")) {
                timeValueMorning.add(timeValueList[i])
            } else {
                timeValueAfternoon.add(timeValueList[i])
            }
        }
        setMorningRecyclerViewVisiblity()
        setNoonRecyclerViewVisiblity()
        text_slots_mor.text = timeValueMorning.size.toString() + " Slots Available"
        text_slots_noon.text = timeValueAfternoon.size.toString() + " Slots Available"
        setListener()
        bookingAdapterMorning = BookingAdapter(timeValueList = timeValueMorning)
        bookingAdapterNoon = BookingAdapter(timeValueList = timeValueAfternoon)
        recyclerViewMorning.adapter = bookingAdapterMorning
        recyclerViewNoon.adapter = bookingAdapterNoon


    }

    private fun setListener() {
        expandView2.setOnClickListener {
            if (recyclerViewNoon.visibility == View.VISIBLE) {
                recyclerViewNoon.visibility = View.GONE
            } else {
                recyclerViewNoon.visibility = View.VISIBLE
            }
            setNoonRecyclerViewVisiblity()
        }

        expandView.setOnClickListener {
            if (recyclerViewMorning.visibility == View.VISIBLE) {
                recyclerViewMorning.visibility = View.GONE
            } else {
                recyclerViewMorning.visibility = View.VISIBLE
            }
            setMorningRecyclerViewVisiblity()
        }
    }

    private fun setNoonRecyclerViewVisiblity() {
        if (recyclerViewNoon.visibility == View.VISIBLE) {
            expandView2.setImageDrawable(downIcon)
        } else {
            expandView2.setImageDrawable(upIcon)
        }

    }

    fun setMorningRecyclerViewVisiblity() {
        if (recyclerViewMorning.visibility == View.VISIBLE) {
            expandView.setImageDrawable(downIcon)
        } else {
            expandView.setImageDrawable(upIcon)
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(timeValueList: ArrayList<TimeValue>) = BookingFragment(timeValueList)
    }
}