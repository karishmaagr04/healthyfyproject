package com.example.healthyfyproject.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BookingResponse(

    @SerialName("slot_id")
    val slotId: Int,

    @SerialName("end_time")
    val endTime: String? = null,

    @SerialName("is_booked")
    val isBooked: Boolean? = null,

    @SerialName("is_expired")
    val isExpired: Boolean? = null,

    @SerialName("start_time")
    val startTime: String? = null,

    @SerialName("username")
    val username: String? = null
)