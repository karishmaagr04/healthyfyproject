package com.example.healthyfyproject.model

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class TimeValue(
    val startTime: String,
    val endTime: String
)
