package com.example.healthyfyproject

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.healthyfyproject.model.TimeValue


@RequiresApi(Build.VERSION_CODES.N)
class DatesPagerAdapter(
    fm: FragmentManager,
    private val map: HashMap<String, ArrayList<TimeValue>>
) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val titles by lazy { ArrayList<String>() }
    private val timeValueList by lazy { ArrayList<ArrayList<TimeValue>>() }
    private val fragments by lazy {
        ArrayList<Fragment>()
    }

    init {
        map.forEach { t, u ->
            titles.add(t)
            timeValueList.add(u)
        }
    }

    override fun getCount(): Int {
        return map.size
    }

    override fun getItem(position: Int): Fragment {
        return BookingFragment.newInstance(timeValueList[position])
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }

}