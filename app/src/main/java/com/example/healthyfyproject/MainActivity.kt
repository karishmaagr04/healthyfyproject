package com.example.healthyfyproject

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.healthyfyproject.helper.SQLiteDatabaseHandler
import com.example.healthyfyproject.helper.TimeUtils
import com.example.healthyfyproject.model.BookingResponse
import com.example.healthyfyproject.model.TimeValue
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var bookingViewModel : BookingViewModel? = null
    private var bookingData: List<BookingResponse>? = null
    private var sqLiteDatabaseHandler: SQLiteDatabaseHandler? = null
    private var map: HashMap<String, ArrayList<TimeValue>> = HashMap()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bookingViewModel = ViewModelProviders.of(this).get(BookingViewModel::class.java)
        bookingViewModel?.getBookingResponse()
        setObserver()
    }

    private fun setObserver() {
        bookingViewModel?.getBookingData()?.observe(this, Observer {
            it.let { bookingResponse ->
                map.putAll(TimeUtils.findDateAndTimeMapFromString(bookingResponse))
                setAdapter()
            }


        })
    }

    private fun setAdapter() {
        bookingPager?.adapter = DatesPagerAdapter(
            supportFragmentManager, map
        )
        tabLayout?.setupWithViewPager(bookingPager)
        bookingPager?.currentItem = 0

    }
}
