package com.example.healthyfyproject

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.healthyfyproject.helper.RetrofitClientInstance
import com.example.healthyfyproject.model.BookingResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BookingViewModel : ViewModel() {

    private val bookingData by lazy {
        MutableLiveData<List<BookingResponse>>()
    }

    fun getBookingData(): LiveData<List<BookingResponse>> = bookingData

    fun getBookingResponse() {
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = withContext(Dispatchers.IO) {
                    RetrofitClientInstance.retrofit.create(BookingService::class.java)
                        .getBookingData()
                }
                if (response.isSuccessful) {
                    bookingData.value = response.body()
                }
            } catch (e: Exception) {
                // we can catch exception here
                e.printStackTrace()
            }
        }
    }

}