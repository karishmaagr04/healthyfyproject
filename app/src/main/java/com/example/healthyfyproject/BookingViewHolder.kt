package com.example.healthyfyproject

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.healthyfyproject.model.TimeValue
import kotlinx.android.synthetic.main.child_item_layout.view.*

class BookingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(timeValue: TimeValue) {
        with(itemView) {
            text_timeSlot.text = timeValue.startTime + " - " + timeValue.endTime
        }
    }

}