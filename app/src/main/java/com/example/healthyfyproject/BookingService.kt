package com.example.healthyfyproject

import com.example.healthyfyproject.model.BookingResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import java.net.URLEncoder

interface BookingService {

    @GET("/api/v2/booking/slots/all")
    suspend fun getBookingData(
        @Query("expert_username",encoded = true) expertUsername: String = "sakshi.sharma%40healthifyme.com",
        @Query("username",encoded = true) username: String = "hme-testmnn1937%40example.com",
        @Query("api_key") apiKey: String = "ea2b9e93ae899eb8f63f6dcc5995ef6409bf15f3"
    ): Response<List<BookingResponse>>

}